package com.hmdp.service;

import com.hmdp.dto.Result;
import com.hmdp.entity.Blog;
import com.baomidou.mybatisplus.extension.service.IService;


public interface IBlogService extends IService<Blog> {
    Result queryById(String id);


    Result queryRecords(Integer current);

    Result isLikeBlog(Long id);


    Result queryBlogLikes(Long id);

    Result saveBlog(Blog blog);

    Result queryBlogFollowersPush(Long lastId, Long offset);

    Result queryBlogFollowersPull(Long lastId, Long offset);

}
