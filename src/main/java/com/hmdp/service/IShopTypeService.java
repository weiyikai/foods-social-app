package com.hmdp.service;

import com.hmdp.dto.Result;
import com.hmdp.entity.ShopType;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;


public interface IShopTypeService extends IService<ShopType> {

    List<ShopType> selectByAsc(String sort);

    Result queryShopByTypeId(Integer typeId, Integer current, Double x, Double y);
}
