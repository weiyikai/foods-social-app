package com.hmdp.service.impl;


import com.hmdp.dto.Result;
import com.hmdp.entity.VoucherOrder;
import com.hmdp.mapper.VoucherOrderMapper;
import com.hmdp.service.ISeckillVoucherService;
import com.hmdp.service.IVoucherOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.utils.RedisIdWorker;
import com.hmdp.utils.UserHolder;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.connection.stream.*;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.hmdp.config.SeckillQueueConfig.X_EXCHANGE;
import static com.hmdp.utils.RedisConstants.SECKILL_BEGIN_TIME_KEY;
import static com.hmdp.utils.RedisConstants.SECKILL_END_TIME_KEY;

@Slf4j
@Service
public class VoucherOrderServiceImpl extends ServiceImpl<VoucherOrderMapper, VoucherOrder> implements IVoucherOrderService {



    @Resource
    private ISeckillVoucherService iSeckillVoucherService;

    @Resource
    private RedissonClient redissonClient;

    @Resource
    private RedisIdWorker redisIdWorker;

    @Resource
    StringRedisTemplate stringRedisTemplate;

    IVoucherOrderService proxy;

    @Resource
    private RabbitTemplate rabbitTemplate;

    /*//阻塞队列
    private BlockingQueue<VoucherOrder> orderTasks =new ArrayBlockingQueue<>(1024 * 1024);*/


    //异步处理线程池
  /*  private static final ExecutorService SECKILL_ORDER_EXECUTOR = Executors.newSingleThreadExecutor();*/

    //lua脚本
    private static final DefaultRedisScript<Long> SECKILL_SCRIPT;

    static {
        SECKILL_SCRIPT = new DefaultRedisScript<>();
        SECKILL_SCRIPT.setLocation(new ClassPathResource("seckill.lua"));
        SECKILL_SCRIPT.setResultType(Long.class);
    }
    //在类初始化之后执行，因为当这个类初始化好了之后，随时都是有可能要执行的
    /*@PostConstruct
    private void init() {
        SECKILL_ORDER_EXECUTOR.submit(new VoucherOrderHandler());
    }*/
    // 用于线程池处理的任务
// 当初始化完毕后，就会去从对列中去拿信息
   /* private class VoucherOrderHandler implements Runnable {

        @Override
        public void run() {
            while (true) {
                try {
                    // 1.获取队列中的订单信息       //阻塞队列
                    VoucherOrder voucherOrder = orderTasks.take();
                    // 2.处理订单
                    handleVoucherOrder(voucherOrder);
                } catch (Exception e) {
                    log.error("处理订单异常", e);
                }
            }
        }*/
        /*private class VoucherOrderHandler implements Runnable {
            String queueName = "stream.orders";

            @Override
            public void run() {
                while (true) {
                    try {
                        // 1.获取消息队列中的订单信息 XREADGROUP GROUP g1 c1 COUNT 1 BLOCK 2000 STREAMS s1 >
                        List<MapRecord<String, Object, Object>> list = stringRedisTemplate.opsForStream().read(
                                Consumer.from("g1", "c1"),
                                StreamReadOptions.empty().count(1).block(Duration.ofSeconds(2)),
                                StreamOffset.create(queueName, ReadOffset.lastConsumed())
                        );
                        // 2.判断订单信息是否为空
                        if (list == null || list.isEmpty()) {
                            // 如果为null，说明没有消息，继续下一次循环
                            continue;
                        }
                        // 解析数据
                        MapRecord<String, Object, Object> record = list.get(0);
                        Map<Object, Object> value = record.getValue();
                        VoucherOrder voucherOrder = BeanUtil.fillBeanWithMap(value, new VoucherOrder(), true);
                        // 3.创建订单
                        handleVoucherOrder(voucherOrder);
                        // 4.确认消息 XACK
                        stringRedisTemplate.opsForStream().acknowledge(queueName, "g1", record.getId());

                    } catch (Exception e) {
                        log.error("处理订单异常", e);
                        handlePendingList();
                    }
                }
            }
            }*/

        /*private class VoucherOrderHandler implements Runnable {

            @Override
            public void run() {
                while (true) {
                    try {
                        //异步队列监听订单消息
                        VoucherOrder voucherOrder = (VoucherOrder) rabbitTemplate.receiveAndConvert("QA");
                        if(voucherOrder!=null)
                        {
                            //执行创建订单至数据库的方法
                            handleVoucherOrder(voucherOrder);
                        } else{
                            continue;
                        };
                    } catch (Exception e) {
                        log.error("处理订单异常", e);
                    }
                }
            }
        }*/

        /**
         * 消息未确认时，进行处理
         */
        /*private void handlePendingList()  {
            while (true) {
                try {
                    // 1.获取消息队列中的订单信息 XREADGROUP GROUP g1 c1 COUNT 1 BLOCK 2000 STREAMS s1 >
                    List<MapRecord<String, Object, Object>> list = stringRedisTemplate.opsForStream().read(
                            Consumer.from("g1", "c1"),
                            StreamReadOptions.empty().count(1),
                            StreamOffset.create(queueName, ReadOffset.from("0"))
                    );
                    // 2.判断订单信息是否为空
                    if (list == null || list.isEmpty()) {
                        // 如果为null，说明消息已经全部确认，跳出循环
                        break;
                    }
                    // 解析数据
                    MapRecord<String, Object, Object> record = list.get(0);
                    Map<Object, Object> value = record.getValue();
                    VoucherOrder voucherOrder = BeanUtil.fillBeanWithMap(value, new VoucherOrder(), true);
                    // 3.创建订单
                    handleVoucherOrder(voucherOrder);
                    // 4.确认消息 XACK
                    stringRedisTemplate.opsForStream().acknowledge(queueName, "g1", record.getId());

                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }*/


    /**
     * 秒杀业务代码
     * @param voucherId
     * @return
     */

    @Override
    public Result seckillVoucher(Long voucherId) {
        //用户id
        Long userId = UserHolder.getUser().getId();
        //生成订单id
        long orderId = redisIdWorker.nextId("order");
        //先进行秒杀时判
        String beginTime = stringRedisTemplate.opsForValue().get(SECKILL_BEGIN_TIME_KEY + voucherId);
        LocalDateTime beginDateTime = LocalDateTime.parse(beginTime);
        if(beginDateTime.isAfter(LocalDateTime.now())){
            return Result.fail("秒杀还未开始");
        }
        String endTime = stringRedisTemplate.opsForValue().get(SECKILL_END_TIME_KEY + voucherId);
        LocalDateTime endDateTime = LocalDateTime.parse(endTime);
        if(endDateTime.isBefore(LocalDateTime.now())){
            return Result.fail("秒杀已经结束");
        }
        //执行lua脚本
        Long result = stringRedisTemplate.execute(
                SECKILL_SCRIPT,
                Collections.emptyList(),
                voucherId.toString(), userId.toString(), String.valueOf(orderId)
        );
        assert result != null;
        int r = result.intValue();
        if(r != 0){
            return Result.fail("不可重复下单！");
        }
        //创建订单
        VoucherOrder voucherOrder = new VoucherOrder();
        voucherOrder.setId(orderId);
        voucherOrder.setUserId(userId);
        voucherOrder.setVoucherId(voucherId);

//        //3.获取代理对象
        proxy = (IVoucherOrderService) AopContext.currentProxy();
        //将订单对象序列化为json字符出串
        /*String voucherOrderStr = JSONUtil.toJsonStr(voucherOrder);*/
        //将订单存入阻塞队列中
        /*orderTasks.add(voucherOrder);*/
        //使用rabbitmq队列,这是生产者，负责发送消息
        rabbitTemplate.convertAndSend(X_EXCHANGE,"XA",voucherOrder);
        //this.handleVoucherOrder(voucherOrder);
        //将订单id返回给前端
        return Result.ok(orderId);


    }


    /*
        @Override
        public Result seckillVoucher(Long voucherId) {
            //查询优惠券信息
            SeckillVoucher seckillVoucher = iSeckillVoucherService.getById(voucherId);
            if(seckillVoucher.getBeginTime().isAfter(LocalDateTime.now())){
                return Result.fail("秒杀尚未开始");
            }
            if(seckillVoucher.getEndTime().isBefore(LocalDateTime.now())){
                return Result.fail("秒杀已经结束");
            }
            Integer stock = seckillVoucher.getStock();
            if(stock < 1){
                return Result.fail("库存不足");
            }
            //获取用户id值
            Long userId = UserHolder.getUser().getId();
            //对一人一单的业务加锁
            //使用redission去获取分布式锁
            RLock lock = redissonClient.getLock("lock:order" + userId);
            boolean islock = lock.tryLock();
            if(!islock){
                return Result.fail("用户已经购买过一次！");
            }
            try {
                //获取代理对象
                IVoucherOrderService proxy = (IVoucherOrderService) AopContext.currentProxy();
                return proxy.createVoucherOrder(voucherId,userId);
            }finally {
                //释放锁
                lock.unlock();
            }
        }*/


    /**
     * 处理订单的方法
     * @param voucherOrder
     */
    public void handleVoucherOrder(VoucherOrder voucherOrder) {

        //这里是做了一个双重的保障，在下单到数据库的时候又进行了判断，是否出现重复下单的情况，可以不做这个操作
        //1.获取用户
        Long userId = voucherOrder.getUserId();
        // 2.创建锁对象
        RLock redisLock = redissonClient.getLock("lock:order:" + userId);
        // 3.尝试获取锁
        boolean isLock = redisLock.tryLock();
        // 4.判断是否获得锁成功
        if (!isLock) {
            // 获取锁失败，直接返回失败或者重试
            log.error("不允许重复下单！");
            return;
        }
        try {
            //注意：由于是spring的事务是放在threadLocal中，此时的是多线程，事务会失效
            proxy.createVoucherOrder(voucherOrder);
        } finally {
            // 释放锁
            redisLock.unlock();
        }

    }

    @Transactional
    public  Result createVoucherOrder( VoucherOrder voucherOrder) {
        final Long userId = voucherOrder.getUserId();
        final Long voucherId = voucherOrder.getVoucherId();
        final Long orderId = voucherOrder.getId();
        //获取用户id值
        int count = query().eq("user_id", userId).eq("voucher_id", voucherId).count();
        if(count > 0){
            return Result.fail("用户已经购买过一次！");
        }

        //使用乐观锁解决库存超卖问题
        boolean success = iSeckillVoucherService.update()
                .setSql("stock= stock -1")
                .eq("voucher_id", voucherId).gt("stock",0).update(); //where id = ? and stock > 0  加入乐观锁解决超卖问问题
        if (!success) {
            //库存不足
            return Result.fail("库存不足！");
        }
        //将订单保存至数据库
        save(voucherOrder);


        return Result.ok(orderId);
    }


}
