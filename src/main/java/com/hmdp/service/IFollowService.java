package com.hmdp.service;

import com.hmdp.dto.Result;
import com.hmdp.entity.Follow;
import com.baomidou.mybatisplus.extension.service.IService;


public interface IFollowService extends IService<Follow> {

    Result Followed(Long followedId, boolean isFollow);

    Result isFollow(Long followedId);

    Result isCommon(Long targetId);
}
