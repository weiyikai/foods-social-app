package com.hmdp.controller;


import com.hmdp.dto.Result;
import com.hmdp.service.impl.FollowServiceImpl;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


@RestController
@RequestMapping("/follow")
public class FollowController {

    @Resource
    private FollowServiceImpl followService;

    @PutMapping("/{id}/{isFollow}")
    public Result Followed(@PathVariable("id") Long followedId,@PathVariable("isFollow") boolean isFollow){
        return followService.Followed(followedId,isFollow);
    }

    @GetMapping("/or/not/{id}")
    public Result isFollow(@PathVariable("id") Long followedId){
        return followService.isFollow(followedId);
    }

    /**
     * 共同关注功能
     * @param targetId
     * @return
     */
    @GetMapping("/common/{id}")
    public Result isCommon(@PathVariable("id") Long targetId){

        return followService.isCommon(targetId);
    }

}
