package com.hmdp.mapper;

import com.hmdp.entity.Follow;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface FollowMapper extends BaseMapper<Follow> {

    //select user_id from tb_follow where follow_user_id = ?;
    List<Long> selectUserId(@Param("followId") Long followId);
}
