package com.hmdp.mapper;

import com.hmdp.entity.Blog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hmdp.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface BlogMapper extends BaseMapper<Blog> {

    List<User>  getListFiled(@Param("ids") String ids);

    List<Blog> getBlogFiled(@Param("ids") String ids);

}
