package com.hmdp.amqp;

import com.hmdp.entity.VoucherOrder;
import com.hmdp.service.impl.VoucherOrderServiceImpl;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.time.LocalTime;
import static com.hmdp.config.SeckillQueueConfig.*;

/**
 * @author 程序员小魏
 * @create 2023-02-01 14:11
 */
@Component
public class ConsumerListener {

    @Resource
    private VoucherOrderServiceImpl voucherOrderServiceImpl;

    @Resource
    private RabbitTemplate rabbitTemplate;
    //监听正常到达的订单信息
    @RabbitListener(queues = QUEUE_A)
    public void listenWorkQueue1(VoucherOrder voucherOrder) throws InterruptedException {
        System.out.println("消费者1接收到消息：【" + voucherOrder +"】" + LocalTime.now());
        if(voucherOrder!=null)
        {
            //执行创建订单至数据库的方法
            voucherOrderServiceImpl.handleVoucherOrder(voucherOrder);
        }
    }
    //监听的消息因为消息堆积到达了死信队列
    @RabbitListener(queues = DEAD_LETTER_QUEUE)
    public void listenWorkQueue2(VoucherOrder voucherOrder) throws InterruptedException {
        System.err.println("消费者2........接收到消息：【" +  voucherOrder + "】" + LocalTime.now());

        // todo 将消息放入redis中进行存储，然后开一个定时任务，将redis中的消息再次放入正常队列中

        rabbitTemplate.convertAndSend(X_EXCHANGE,"XA",voucherOrder);
    }
}
