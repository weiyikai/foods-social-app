package com.hmdp.utils;

import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import jodd.util.StringUtil;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static com.hmdp.utils.RedisConstants.*;

/**
 * @author 程序员小魏
 * @create 2022-12-13 14:47
 */
@Component
public class CacheClientSelf {


    private final StringRedisTemplate stringRedisTemplate;

    public CacheClientSelf(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }

    /**
     * 将任意Java对象序列化为json，并设置TTL过期时间
     * @param key
     * @param value
     * @param time
     * @param unit
     */
    public void set(String key,Object value,Long time,TimeUnit unit){
        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(value),time, unit);
    }

    /**
     * 将任意Java对象序列化为Json，并设置TTL过期时间，用于处理缓存击穿问题
     * @param key
     * @param value
     * @param time
     * @param unit
     */
    public void setWithLogicalTimeout(String key,Object value,Long time,TimeUnit unit) {
        RedisData redisData = new RedisData();
        redisData.setData(value);
        redisData.setExpireTime(LocalDateTime.now().plusSeconds(unit.toSeconds(time)));
        stringRedisTemplate.opsForValue().set(key,JSONUtil.toJsonStr(redisData));
    }

    /**
     *  解决缓存穿透问题，缓存null方法
     * @param id
     * @return
     */
    public <R,ID> R  queryWithPassThrough(String key, ID id, Class<R> type, Function<ID,R> function){
        //1.根据商铺的id查询redis中是否有商铺
        String shopJson = stringRedisTemplate.opsForValue().get(key);
        if(StringUtil.isNotBlank(shopJson)){
            //2.如果商品命中，先把字符串转换为对象，然后直接返回
            return JSONUtil.toBean(shopJson, type);
        }
        //要增加判断，如果为空值，那么直接返回
        if(shopJson != null){
            return null;
        }
        //3.如果商铺未命中，查询数据库
        R r = function.apply(id);

        if(r == null){
            /*return Result.fail("店铺不存在");*/
            //为了解决缓存穿透的问题，这里要返回空值给缓存
            stringRedisTemplate.opsForValue().set(key,"", CACHE_NULL_TTL, TimeUnit.MINUTES);
            return null;
        }
        stringRedisTemplate.opsForValue().set(key,JSONUtil.toJsonStr(r), CACHE_SHOP_TTL , TimeUnit.MINUTES);
        return r;
    }

    private static final ExecutorService CACHE_REBUILD_EXECUTOR = Executors.newFixedThreadPool(10);

    /**
     * 设置缓存过期时间解决缓存击穿问题 (这个方法可以解决三种缓存问题了）
     * @param id
     * @return
     */
    public <R,ID>  R queryWithLogicalExpire(String key, ID id, Class<R> type, Function<ID,R> function) {
        //1.根据商铺的id查询redis中是否有商铺
        String shopJson = stringRedisTemplate.opsForValue().get(key);
        if(StringUtil.isBlank(shopJson)){
            //------------缓存null值解决缓存穿透问题--------------
            //2.1未命中
            /*return null;*/
            //要增加判断，如果为空值，那么直接返回
            if(shopJson != null){
                return null;
            }
            //如果商铺未命中，查询数据库
            R r = function.apply(id);
            if(r == null){
                /*return Result.fail("店铺不存在");*/
                //为了解决缓存穿透的问题，这里要返回空值给缓存
                stringRedisTemplate.opsForValue().set(key,"", CACHE_NULL_TTL, TimeUnit.MINUTES);
                return null;
            }
            //如果数据库中存在，那么放入redis缓存中
            setWithLogicalTimeout(RedisConstants.CACHE_SHOP_KEY + id, r,10L, TimeUnit.SECONDS);
            return r;
        }
        //2.2 命中判断缓存是否过期
        RedisData redisData = JSONUtil.toBean(shopJson, RedisData.class);
        //返回值，如店铺信息
        R r = JSONUtil.toBean((JSONObject) redisData.getData(), type);
        LocalDateTime expireTime = redisData.getExpireTime();
        if(expireTime.isAfter(LocalDateTime.now())) {
            // 2.3.未过期，直接返回店铺信息
            return r;
        }
        //2.4.已过期，尝试获取互斥锁
        /*String lockKey = LOCK_SHOP_KEY + id;*/
        boolean mutex = getMutex(id);

        if(mutex){
            //3.1.获取锁成功,再次判断缓存中的数据是否过期
            if(expireTime.isAfter(LocalDateTime.now())) {
                // 未过期，直接返回店铺信息
                return r;
            }
            CACHE_REBUILD_EXECUTOR.submit(()-> {
                //3.2开启独立的线程，缓存重建
                try {
                    //查询数据库
                    R r2 = function.apply(id);
                    setWithLogicalTimeout(RedisConstants.CACHE_SHOP_KEY + id, r2,10L, TimeUnit.SECONDS);
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }finally {
                    ReleaseMutex(id);
                }

            });

        }
        return r;
    }

    /**
     * 互斥锁解决缓存击穿问题
     * @param keyPrefix
     * @param id
     * @param type
     * @param dbFallback
     * @param time
     * @param unit
     * @param <R>
     * @param <ID>
     * @return
     */
    public <R, ID> R queryWithMutex(
            String keyPrefix, ID id, Class<R> type, Function<ID, R> dbFallback, Long time, TimeUnit unit) {
        String key = keyPrefix + id;
        // 1.从redis查询商铺缓存
        String shopJson = stringRedisTemplate.opsForValue().get(key);
        // 2.判断是否存在
        if (StrUtil.isNotBlank(shopJson)) {
            // 3.存在，直接返回
            return JSONUtil.toBean(shopJson, type);
        }
        // 判断命中的是否是空值
        if (shopJson != null) {
            // 返回一个错误信息
            return null;
        }

        // 4.实现缓存重建
        // 4.1.获取互斥锁
        String lockKey = LOCK_SHOP_KEY + id;
        R r = null;
        try {
            boolean isLock = tryLock(lockKey);
            // 4.2.判断是否获取成功
            if (!isLock) {
                // 4.3.获取锁失败，休眠并重试
                Thread.sleep(50);
                return queryWithMutex(keyPrefix, id, type, dbFallback, time, unit);
            }
            // 4.4.获取锁成功，根据id查询数据库
            r = dbFallback.apply(id);
            // 5.不存在，返回错误
            if (r == null) {
                // 将空值写入redis
                stringRedisTemplate.opsForValue().set(key, "", CACHE_NULL_TTL, TimeUnit.MINUTES);
                // 返回错误信息
                return null;
            }
            // 6.存在，写入redis
            this.set(key, r, time, unit);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }finally {
            // 7.释放锁
            unlock(lockKey);
        }
        // 8.返回
        return r;
    }

    private boolean tryLock(String key) {
        Boolean flag = stringRedisTemplate.opsForValue().setIfAbsent(key, "1", 10, TimeUnit.SECONDS);
        return BooleanUtil.isTrue(flag);
    }

    private void unlock(String key) {
        stringRedisTemplate.delete(key);
    }


    /**
     * 获取互斥锁
     * @param id
     * @return 成功则返回true
     */
    private <ID> boolean getMutex(ID id){
        String key = LOCK_SHOP_KEY + id;
        Boolean flag = stringRedisTemplate.opsForValue().setIfAbsent(key, "1", LOCK_SHOP_TTL, TimeUnit.SECONDS);
        return BooleanUtil.isTrue(flag);
    }

    /**
     * 释放互斥锁
     * @param id
     */
    private <ID> boolean ReleaseMutex(ID id){
        String key = LOCK_SHOP_KEY + id;
        Boolean delete = stringRedisTemplate.delete(key);
        return BooleanUtil.isTrue(delete);
    }
}
