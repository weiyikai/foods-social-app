package com.hmdp.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
@ComponentScan
@Configuration
public class RedissonConfig {

    @Bean
    public RedissonClient redissonClient(){
        // 配置
        Config config = new Config();
//        config.useClusterServers()
//                .setScanInterval(2000) // 集群状态扫描间隔时间，单位是毫秒
//                //可以用"rediss://"来启用SSL连接
//                .addNodeAddress("redis://192.168.200.130:7001", "redis://192.168.200.130:7002" , "redis://192.168.200.130:7003")
//                .addNodeAddress("redis://192.168.200.130:8001" ,"redis://192.168.200.130:8002" , "redis://192.168.200.130:8002");
        config.useSingleServer().setAddress("redis://192.168.200.130:6379");
        // 创建RedissonClient对象
        return Redisson.create(config);
    }
}
